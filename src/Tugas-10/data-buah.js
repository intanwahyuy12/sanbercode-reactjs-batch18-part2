import React from 'react';

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class DataBuah extends React.Component {
  render() {
    return (
      <div style={{width: "35%", padding: "5px", margin: "10px auto"}}>
		<h1>Tabel Harga Buah</h1>
        <table style={{border: "1px solid", width: "100%"}}>
			<tr style={{"background-color": "DarkGray", "font-weight": "bold"}}>
				<td>Nama</td>
				<td>Harga</td>
				<td>Berat</td>
			</tr>
			{dataHargaBuah.map(item=> {
			  return (
				<tr style={{"background-color": "Coral", "text-align": "left"}}>
				  <td>{item.nama}</td>
				  <td>{item.harga}</td>
				  <td>{item.berat/1000} kg</td>
				</tr>
			  )
			})}
		</table>
      </div>
    )
  }
}

export default DataBuah;
