import React from 'react';
import './App.css';
import FormBuah from './Tugas-9/form-buah';
import DataBuah from './Tugas-10/data-buah';

function App() {
  return (
    <div className="App">
      <FormBuah />
      <DataBuah />
    </div>
  );
}

export default App;