import React from 'react';

const pad = {padding: "20px"};

class FormBuah extends React.Component {
  render() {
    return (
      <form style={{border: "1px solid", "border-radius": "10px", width: "35%", padding: "5px", margin: "10px auto"}}>
			<h1>Form Pembelian Buah</h1>
			<table style={{"text-align": "left"}}>
				<tr>
					<td><b>Nama Pelanggan</b></td>
					<td><input type="text"/></td>
				</tr>
				<tr>
					<td><b>Daftar Item</b></td>
					<td>
						<label>
						  <input
							name="semangka"
							type="checkbox"
						  />
						  Semangka
						</label><br />
						<label>
						  <input
							name="jeruk"
							type="checkbox"
						  />
						  Jeruk
						</label><br />
						<label>
						  <input
							name="nanas"
							type="checkbox"
						  />
						  Nanas
						</label><br />
						<label>
						  <input
							name="salak"
							type="checkbox"
						  />
						  Salak
						</label><br />
						<label>
						  <input
							name="anggur"
							type="checkbox"
						  />
						  Anggur
						</label>
					</td>
				</tr>
				<tr>
                    <button>
                    <a href="#" style={{"text-decoration": "none", color: "black"}}>Kirim</a>
                    </button>
				</tr>
			</table>
		</form>
    )
  }
}

export default FormBuah;
